document.addEventListener('DOMContentLoaded', function() {

	window.XKB = {};
	window.XKB.saveChanges = function() {
		var dataElm = document.getElementById('data');

		// Get a value saved in a form.
		var data = dataElm.value;
		console.log(data);
		// Check that there's some code there.
		if (!data) {
			return;
		}
		// Save it using the Chrome extension storage API.
		chrome.storage.sync.set({'maps': data});
	}
	window.XKB.loadMaps = function() {
		chrome.storage.sync.get('maps', function(obj){
			var dataElm = document.getElementById('data');
			dataElm.value = obj.maps;
			console.log(obj);
		});
	}

	/**
	 * Adds a map set (key mappings to apply when url matches given pattern).
	 *
	 * @param	data
	 *         Map set object:
	 *         {
	 *           pattern: <regex pattern to apply against window.location>,
	 *           maps:    Array of key map objects ({meta: ..., key: ...})
	 *         }
	 */
	window.XKB.addMapSet = function(data) {
		var d = document;
		var container = d.getElementById('maps');
		var mapSetContainer = d.createElement('div');
		mapSetContainer.innerHTML = window.XKB.mapSetTemplate;
		console.log(data);
		if(data !== undefined) {
			var patternElm = mapSetContainer.querySelector('[name="pattern[]"]');
			patternElm.value = data.pattern;
			for (var i = 0; i++; i < data.maps.length) {
				window.XKB.addMapSetMap(container, data.maps[i]);
			}
		} else {
			window.XKB.addMapSetMap(mapSetContainer);
		}
		container.appendChild(mapSetContainer);
		var addMapBtn = container.querySelector('[type="button"]');
		addMapBtn.addEventListener('click', function() {
			window.XKB.addMapSetMap(container);
		});
	}

	/**
	 * Adds a key mapping to a map set.
	 *
	 * @param container
	 *  			 DOM element of containing map set
	 * @param map
	 * 				 Key map object ({meta: ..., key: ...})
	 */
	window.XKB.addMapSetMap = function(container, map) {
		// Get container in which to add new map set.
		var mapsContainer = container.querySelector('.map-set-maps');

		// Create container for new map set.
		var mapContainer = document.createElement('tbody');
		mapContainer.setAttribute('id', 'map-');
		mapContainer.setAttribute('class', 'map-set-map');
		mapContainer.innerHTML = window.XKB.mapTemplate;
		var hideCodeBtn = mapContainer.querySelector('input.hide-code')
		var showCodeBtn = mapContainer.querySelector('input.show-code')
		hideCodeBtn.addEventListener('click', function() {
			console.log('bye');
		});
		showCodeBtn.addEventListener('click', function() {
			console.log('hi');
		});

		if(map !== undefined) {
			var metaElm = mapContainer.querySelector('[name="meta[]"]')
			var keyElm = mapContainer.querySelector('[name="key[]"]')
			metaElm.value = map.meta;
			keyElm.value = map.key;
		}
		mapsContainer.appendChild(mapContainer);
	}

	window.XKB.mapSetTemplate = 
		'<div class="map-set" id="map-set-">' +
		  '<label for="pattern[]">Pattern</label><br>' +
			'<input type="text" class="pattern-value" name="pattern[]" value="">' +
			'<table class="map-set-maps">' +
			'</table>' +
			'<input type="button" value="Add map">';
		'</div>'; 

	window.XKB.mapTemplate = 
		'<tr>' +
			'<td>' +
				'<label for="shift-key[]">Shift</label>' +
				'<input type="checkbox" class="shift-key-value" name="shift-key[]">' + 
			'</td>' +
			'<td>' +
				'<label for="ctrl-key[]">Ctrl</label>' +
				'<input type="checkbox" class="ctrl-key-value" name="ctrl-key[]">' + 
			'</td>' +
			'<td>' +
				'<label for="alt-key[]">Alt</label>' +
				'<input type="checkbox" class="alt-key-value" name="alt-key[]">' + 
			'</td>' +
			'<td>' +
				'<label for="key[]">Key</label>' +
				'<input type="text" class="key-value" name="key[]">' + 
			'</td>' +
			'<td>' +
				'<label for="label[]">Label</label>' +
				'<input type="text" class="label-value" name="label[]">' + 
			'</td>' +
		'</tr>' +
		'<tr class="collapse">' +
			'<td colspan="5">' +
				'<div>' +
					'<div class="collapse-show"><input type="button" class="show-code" value="Code"></div>' +
					'<div class="collapse-hide"><input type="button" class="hide-code" value="Hide code"></div>' +
				'</div>' +
				'<textarea class="code-value collapse-hide" name="code[]"></textarea>' + 
			'</td>' +
		'</tr>';

	

  var saveButton = document.getElementById('save-data');
  saveButton.addEventListener('click', function() {
		window.XKB.saveChanges; 
	});
  var addButton = document.getElementById('add-map-set');
  addButton.addEventListener('click', function() {
		window.XKB.addMapSet(); 
	});
	// var dataArea = document.getElementById('data');
	// dataArea.innerHTML = window.XKB.loadMaps();

}, false);
